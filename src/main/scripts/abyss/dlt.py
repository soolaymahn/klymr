#!/usr/bin/python3

from elasticsearch import Elasticsearch

from elasticsearch.helpers import bulk
import sys

es = Elasticsearch(["159.203.246.62"])


def get_total():
    res = es.search(index="klymr", doc_type='idx', body={
        "query": {
            "term": {"tags": "##"}
        }})
    return res['hits']['total']


def get(size):
    res = es.search(index="klymr", doc_type='idx', size=size, body={
        "sort": [{"_timestamp": {"order": "asc"}}],
        "query": {
            "term": {"tags": "##"}
        }
    })
    return res['hits']['hits']

if __name__ == "__main__":
    hh_max = 20

    del_count = get_total() - hh_max

    deletes = []

    if del_count > 0:
        for s in get(del_count):
            s['_op_type'] = 'delete'
            deletes.append(s)

    bulk(es, deletes)
