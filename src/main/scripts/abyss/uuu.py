#!/usr/bin/python3

import json
import urllib.request
import requests
from random import randint
import heapq

import time
from azure.storage.table import TableService, Entity
from azure.storage.queue import QueueService

from elasticsearch import Elasticsearch

from dateutil.parser import parse as parse_date


def gulp(url):
    response = urllib.request.urlopen(url).read().decode('utf8')
    data = json.loads(response)
    if 'data' not in data:
        print(data)
        return None, None
    if 'paging' not in data or 'next' not in data['paging']:
        return data['data'], None
    return data['data'], data['paging']['next']


def read(url):
    response = urllib.request.urlopen(url).read().decode('utf8')
    data = json.loads(response)
    return data


class FBS:
    def __init__(self, at, se, id):
        self.access_token = at
        self.since = se
        self.id = id

    def read_page(self, like):
        return "https://graph.facebook.com/v2.6/" + like + "?" \
                    "access_token=" + self.access_token

    def gulp_feed(self, like):
        return "https://graph.facebook.com/v2.6/" + like + "/feed?" \
                "access_token=" + self.access_token + \
                "&since=" + self.since + \
                "&fields=from,type,object_id"

    def read_photo(self, obid):
        return "https://graph.facebook.com/v2.6/" + obid + "?" \
                "access_token=" + self.access_token + \
                "&fields=link,images,created_time"

    def read_photo_likes(self, obid):
        return "https://graph.facebook.com/v2.6/" + obid + "/likes?" \
                "access_token=" + self.access_token + \
                "&summary=true" + \
                "&fields=link,images"

    def has_user_like(self, obid):
        return read(self.read_photo_likes(obid))['summary']['has_liked']


class PS:
    def __init__(self, fbs, lid):
        self.page_id = lid
        self.nxt = fbs.gulp_feed(self.page_id)

        self.name = read(fbs.read_page(self.page_id))['name']

    def __iter__(self):
        return self

    def __next__(self):
        data, nt = gulp(self.nxt)
        if not (data and nt):
            raise StopIteration
        self.nxt = nt
        return data


class ATSBD:
    def __init__(self, acc, key):
        self.tas = TableService(account_name=acc, account_key=key)
        self.qas = QueueService(account_name=acc, account_key=key)

    def get_likes(self, userid):
        likes = {}
        for page in self.tas.query_entities('channels', filter="PartitionKey eq '" + userid + "'"):
            likes[page['RowKey']] = page['Tags']
        return likes

    def get_ts(self):
        return str(self.tas.get_entity('since', 'ACTIVE', '0')['ts'])

    def set_ts(self, nts):
        self.tas.insert_or_replace_entity('since',  {'PartitionKey': 'ACTIVE', 'RowKey': '0', 'ts': nts})

    def get_image(self, userid, obid):
        try:
            return self.tas.get_entity('ghost', userid, obid)
        except:
            return False

    def add_image(self, userid, obid, chid, status, kid):
        self.tas.insert_or_replace_entity('ghost', {"PartitionKey": userid, "RowKey": obid, "ChannelId": chid, "KlymrId": kid, "Status": status})


class ES:
    def __init__(self, addr):
        self.es = Elasticsearch([addr])

    def get_likes(self, userid):
        likes = {}

        res = self.user_likes(userid)

        for like in res['hits']['hits']:
            likes[like['_source']['channelId']] = " ".join(like['_source']['tags'])

        return likes

    def get_rates(self, userid):
        rates = {}

        res = self.user_likes(userid)

        for like in res['hits']['hits']:
            rates[like['_source']['channelId']] = like['_source']['rate']

        return rates

    def user_likes(self, userid):
        return self.es.search(index="klymr", doc_type='fbc', size=100, body={
            "sort": [{"_timestamp": {"order": "asc"}}],
            "query": {
                "term": {"userId": userid}
            }
        })

    def delete(self, kid):
        try:
            self.es.delete(index="klymr", doc_type='idx', id=kid)
        except:
            pass


def klymr_save(userid, chid, obid, urls, desc):
    r = requests.post('http://klymr.elasticbeanstalk.com/abyss/image',
                      json={"userid": userid, "obid": obid, "chid": chid, "urls": urls, "saved": False, "desc": desc})

    if r.status_code == 200:
        kid = json.loads(r.content.decode('utf-8'))['id']
        print(kid, userid, chid, obid, desc)
        return kid
    print("FAILED", obid)

    return None


def abyss_reduce_entry(store, es, userid, rate, dt, obid, chid, kid, desc, urls, has_like, new_image, new_like):
    if (has_like and (new_image or new_like)) or (new_image and (randint(0, 100) < rate)):
        desc += " #"

    else:
        desc += " ##"

    if kid is not "NULL":
        es.delete(kid)

    if klymr_save(userid, chid, obid, urls, desc) is None:
        store.add_image(userid, obid, chid, "FAILED")


def abyss_reduce(a_map, store, es):
    for userid, images in a_map.items():
        rates = es.get_rates(userid)

        for image in images:
            abyss_reduce_entry(store, es, userid, rates[image[2]], *image)


def map_obid(store, fbs, userid, obid, chid, desc):
    has_like = fbs.has_user_like(obid)
    ghost_entry = store.get_image(userid, obid)
    new_image = (not ghost_entry)

    new_like = (not new_image) and has_like and (ghost_entry['Status'] in {"NEW"})

    if new_like or new_image:
        if new_image:
            kid = "NULL"
        else:
            kid = ghost_entry['KlymrId']

        image = read(fbs.read_photo(obid))
        dt = time.mktime(parse_date(image["created_time"]).timetuple())
        return dt, obid, chid, kid, desc, [img['source'] for img in image["images"]], has_like, new_like, new_image

    return None


def map_page_id(fbs, userid, like, desc):
    try:
        ps = PS(fbs, like)

        print(ps.name, like)

        for data in ps:
            if data:
                for upd in data:
                    if 'type' in upd and upd['type'] == 'photo' and upd['from']['id'] == like:
                        row = map_obid(store, fbs, userid, upd['object_id'], like, desc)
                        if row is not None:
                            yield row

    except Exception as e:
        print(e)


def abyss_map(userids, store, es):
    out = {}

    for userid in userids:
        likes = es.get_likes(userid)

        since = store.get_ts()

        print(since)

        fbs = FBS(access_token, since, userid)

        images = []

        for like in likes.keys():
            for row in map_page_id(fbs, userid, like, likes[like]):
                if len(images) >= 20:
                    if row[0] > images[0][0]:
                        heapq.heapreplace(images, row)
                else:
                    heapq.heappush(images, row)

        out[userid] = images

    return out

if __name__ == "__main__":
    access_token = 'EAAFyRNpRZA1cBAOQlh9wFBnBIxDNakZCDgZCw5pZAZC9aKTaoBAdIZADLlv8OaZBoVhT6hfwOibYsU6Ikdo7qiWOzZCz42ZBoDgjO6IOhI4wsNYcmDSpslYjRAop2yO1RGCbf8A4sRkzm8kDZAvtfkrgNf'

    userids = ["10154215964014922"]

    store = ATSBD('metalmonster', '5Me7n7LIkjzxsYyp5SlMU+8Pa7IbADL0AL4FY6Fq8QC1wdeaYLOmZPBwOIZ/E/SbhkhbGREdCcuUOE8NzRDGvg==')
    es = ES("159.203.246.62")

    map_out = abyss_map(userids, store, es)

    abyss_reduce(map_out, store, es)

    store.set_ts(str(int(time.time()) - 60*60*24))
