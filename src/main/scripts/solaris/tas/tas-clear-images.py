#! /usr/bin/python

from pymongo import MongoClient
from azure.storage.table import TableService, Entity
from azure.storage.queue import QueueService
import time

if __name__ == "__main__":
    tas = TableService(account_name='metalmonster', account_key='5Me7n7LIkjzxsYyp5SlMU+8Pa7IbADL0AL4FY6Fq8QC1wdeaYLOmZPBwOIZ/E/SbhkhbGREdCcuUOE8NzRDGvg==')
    qas = QueueService(account_name='metalmonster', account_key='5Me7n7LIkjzxsYyp5SlMU+8Pa7IbADL0AL4FY6Fq8QC1wdeaYLOmZPBwOIZ/E/SbhkhbGREdCcuUOE8NzRDGvg==')

    tas.delete_table('ghost')

    qas.delete_queue('feed')

    qas.delete_queue('saved')

    qas.delete_queue('unsaved')

    time.sleep(60)

    tas.create_table('ghost')

    qas.create_queue('feed')

    qas.create_queue('saved')

    qas.create_queue('unsaved')
