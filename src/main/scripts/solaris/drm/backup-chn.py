#! /usr/bin/python3

from elasticsearch import Elasticsearch


if __name__ == "__main__":
    es = Elasticsearch(["159.203.246.62"])

    res = es.search(index="klymr", doc_type='fbc', size=100, body={
        "sort": [{"_timestamp": {"order": "asc"}}]
    })

    for line in res['hits']['hits']:

        print(line["_source"]["userId"],
              "FACEBOOK",
              line["_source"]["channelId"],
              line["_source"]["name"].replace(" ", "_"),
              " ".join(line["_source"]["tags"]))
