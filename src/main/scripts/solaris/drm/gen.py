import urllib.request
import json

fbc = {
    "_timestamp": {
        "enabled": True
    },
    "properties": {
        "userId": {
            "type": "string",
            "index": "not_analyzed"
        },
        "channelId": {
            "type": "string",
            "index": "not_analyzed"
        },
        "tags": {
            "type": "string",
            "index": "not_analyzed"
        },
        "name": {
            "type": "string",
            "index": "not_analyzed"
        },
        "rate": {
            "type": "integer"
        },
        "state": {
            "type": "string",
            "index": "not_analyzed"
        }
    }
}

idx = {
    "_timestamp": {
        "enabled": True
    },
    "properties": {
        "userId": {
            "type": "string",
            "index": "not_analyzed"
        },
        "desc": {
            "type": "string",
            "store": True
        },
        "tags": {
            "type": "string",
            "index": "not_analyzed"
        },
        "image_url": {
            "type": "string",
            "index": "not_analyzed"
        },
        "embed_url": {
            "type": "string",
            "index": "not_analyzed"
        },
        "ext_url": {
            "type": "string",
            "index": "not_analyzed"
        }
    }
}

index = "klymr"

try:
    req = urllib.request.Request(url='http://159.203.246.62:9200/' + index + '/',
                                 method='DELETE')

    with urllib.request.urlopen(req) as f:
        print("DELETION", f.status, f.reason)

except:
    pass

req = urllib.request.Request(url='http://159.203.246.62:9200/' + index + '/',
                             method='PUT',
                             headers={'Content-Type': 'application/json'})

with urllib.request.urlopen(req) as f:
    print("CREATION", f.status, f.reason)

req = urllib.request.Request(url='http://159.203.246.62:9200/' + index + '/_mapping/fbc',
                             data=json.dumps(fbc).encode('utf8'),
                             method='PUT',
                             headers={'Content-Type': 'application/json'})

with urllib.request.urlopen(req) as f:
    print("FBC", f.status, f.reason)

req = urllib.request.Request(url='http://159.203.246.62:9200/' + index + '/_mapping/idx',
                             data=json.dumps(idx).encode('utf8'),
                             method='PUT',
                             headers={'Content-Type': 'application/json'})

with urllib.request.urlopen(req) as f:
    print("IDX", f.status, f.reason)
