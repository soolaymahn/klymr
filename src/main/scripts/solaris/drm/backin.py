#! /usr/bin/python3
from elasticsearch import Elasticsearch
import fileinput

from elasticsearch.helpers import bulk
import sys


def get_tags(st):
    return [tag for tag in st.split() if tag[0] == "#"]

if __name__ == "__main__":
    es = Elasticsearch(["159.203.246.62"])

    for i, line in enumerate(open("out/backup-idx")):
        line = line.rstrip().split(" ", 3)

        try:
            if line[1] == "IMAGE":
                    res = es.index(index="klymr",
                                   doc_type="idx",
                                   body={"tags": get_tags(line[3]),
                                         "desc": line[3],
                                         "userId": line[0],
                                         "display": "IMAGE",
                                         "image_url": line[2]})
                    print("IMAGE", res['_id'])
        except:
            print("FAILED", line)

    for i, line in enumerate(open("out/backup-chn")):
        line = line.rstrip().split(" ", 4)

        try:
            if line[1] == "FACEBOOK":
                res = es.index(index="klymr",
                               doc_type="fbc",
                               body={"tags": get_tags(line[4]),
                                     "name": line[3].replace("_", " "),
                                     "userId": line[0],
                                     "channelId": line[2],
                                     "rate": 0,
                                     "state": "ON"})
                print("FBC", res['_id'])

        except:
            print("FAILED", line)