#! /usr/bin/python3
import requests
import warnings
from skimage import io
from skimage.measure import compare_ssim
from skimage.transform import resize
from os import listdir, makedirs
from os.path import isfile, join, exists

if __name__ == "__main__":
    pool = 100

    headers = {"Content-type": "application/json"}
    data = {"size": str(pool), "sort": "Date",  "tags": [], "userId": "10154215964014922"}
    response = requests.post(url="http://klymr.elasticbeanstalk.com/tags/images/urls",
                             headers=headers,
                             json=data)

    chrt = []

    dim = 50

    if not exists("cache"):
        makedirs("cache")

    cache = {f for f in listdir("cache/") if isfile(join("cache/", f))}

    for itm in response.json()['content']:
        if itm['id'] + ".png" in cache:
            img = resize(io.imread("cache/" + itm['id'] + ".png"), (dim, dim))
        else:
            img = resize(io.imread(itm['url']), (dim, dim))

        chrt.append((itm['id'], itm['desc'], img, frozenset(itm['tags'])))

    deep = 10

    for x, itm1 in enumerate(chrt):
        kid1, desc1, img1, tags1 = itm1

        if kid1 + ".png" not in cache:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                io.imsave('cache/' + kid1 + '.png', img1)

        if x >= deep:
            continue

        for y, itm2 in enumerate(chrt[x + 1:]):
            kid2, desc2, img2, tags2 = itm2
            ssim = compare_ssim(img1, img2, multichannel=True)
            if ssim > .95:
                nd = desc2 + " " + " ".join(tags1 - tags2)
                chrt[y + x + 1] = (kid2, nd, img2, tags1 | tags2)
                requests.put("http://klymr.elasticbeanstalk.com/image", json={"id": kid2, "desc": nd})
                requests.delete("http://klymr.elasticbeanstalk.com/image?id=" + kid1)

                print(kid2, kid1, nd)

                break


