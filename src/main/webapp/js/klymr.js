myApp.service('ContentService', function ($http,  $timeout, $rootScope, $q, $location, principal) {
    var imageData = {images:[],tags:[],rqTags:[]};
    var pagesize = 12;
    var usize = 0;
    var rtsize = 0;
    var maxsize = {tags:0, images:0};
    var sort = {urls: "Date", tags:"Hits"};
    var user = principal.getAuth();

    this.updateTagsAndImages = function(){
        var deferred = $q.defer();
        var tags = false;
        var urls = false;
        $rootScope.$broadcast('full_load_start');
        this
            .updateTags()
            .then(function(result){
                $rootScope.$broadcast('tag_load_done');
                tags = true;
                if(urls){
                    deferred.resolve(true);
                }
            });
        this
            .updateUrls()
            .then(function(result){
                $rootScope.$broadcast('url_load_done');
                urls = true;
                if(tags){
                    deferred.resolve(true);
                }
            });
        return deferred.promise;
    };

    this.updateTags = function(){
        var deferred = $q.defer();
        $http.post('/tags/images/tags', {tags:imageData.rqTags, size:rtsize, sort:sort.tags, userId:user.userID})
            .success(function(data){
                imageData.tags = data.content;
                maxsize.tags = data.total;
                deferred.resolve(true);
            });
        return deferred.promise;
    };

    this.updateUrls = function(){
        var deferred = $q.defer();
        $http.post('/tags/images/urls', {tags:imageData.rqTags, size:usize, sort:sort.urls, userId:user.userID})
            .success(function(data){
                imageData.images = data.content;
                maxsize.images = data.total;
                deferred.resolve(true);
            });
        return deferred.promise;
    };



    this.moreUrls = function(){
        var deferred = $q.defer();
        if(usize < maxsize.images){
            usize = usize + pagesize;
            this
                .updateUrls()
                .then(function(result){
                    deferred.resolve(true);
                });
        }
        else{
            deferred.resolve(true);
        }
        return deferred.promise;
    };

    this.setRq = function(data){
        imageData.rqTags = data;
        return this.updateTagsAndImages();
    };

    this.getImageData = function(){
        return imageData;
    };

    this.getAddError = function(){
        return addError;
    };

    this.maxSize = function(){
        return maxsize;
    };

    this.setRTSize = function(size){
        rtsize = size;
    };

    this.getRTSize = function(){
        return rtsize;
    };

    this.getSort = function(){
        return sort;
    };

    this.setUsize = function(size){
        usize = size;
    };

    this.clear = function(){
        imageData.images = [];
        maxsize.images = 0;
        imageData.tags = [];
        maxsize.tags = 0;
    }

});

myApp.service('AddService', function($http, $timeout, ContentService, flowFactory, principal){
    var status = {show:false, link:true, loading: false, saved: false};
    var uploadData = {valid:true, link:"", fileName:"", fileSuccess:false};
    var formData = {url:"", desc:""};
    var addError = {error:false, message:""};
    var user = principal.getAuth();
    var FlowObject = flowFactory.create({});

    this.addNewImage = function(url,desc,onServer){
        var self = this;
        $http.post('/image', {desc: desc, url: url, saved:onServer, userId:user.userID})
            .then(function(response){
                self.addSuccess();
                $timeout(function() {ContentService.updateTagsAndImages()} , 1000);
            }, function(response){
                self.addError(response);
            });
    };

    this.addSuccess = function(){
        status.loading = false;
        status.saved = true;
        addError.error = false;
        addError.message = "";
        formData.desc = "";
        formData.url = "";
        uploadData.link = "";
        uploadData.fileName = "";
        uploadData.fileSuccess = false;
        FlowObject.cancel();
    };

    this.addError = function(reason){
        status.saved = false;
        addError.error = true;
        addError.message = reason.data.message;
        status.loading = false;
    };

    this.submit = function(){
        status.loading = true;
        if(status.link){
            this.addNewImage(formData.url, formData.desc, false);
        }
        else{
            this.addNewImage(uploadData.link, formData.desc, true);
        }
    };

    this.flowSubmit = function () {
        uploadData.fileSuccess = false;
        FlowObject.upload();

    };

    this.showLink = function(val){
        status.link = val;
    };

    this.upSuccess = function($file, $message){
        uploadData.fileName = $file.name;
        uploadData.link = $message;
        uploadData.fileSuccess = true;
    };

    this.checkFile = function($file){
        var ext = !!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()];
        var size = $file.size < 1024 * 1024 * 5;
        uploadData.valid = ext && size;
        if(!uploadData.valid){
            uploadData.link = "";
            uploadData.fileName = "";
        }
        return uploadData.valid;
    };

    this.cancel = function () {
        uploadData.link = "";
        uploadData.fileName = "";
        uploadData.fileSuccess = false;
        status.saved = false;
        addError.error = false;
        addError.message = false;
        FlowObject.cancel();
    };

    this.complete = function() {
        if((uploadData.fileName.length) && (uploadData.link.length))
            uploadData.fileSuccess = true;
    };

    this.getAddError = function(){
        return addError;
    };

    this.getStatus = function(){
        return status;
    };

    this.getUploadData = function(){
        return uploadData;
    };

    this.getFormData = function(){
        return formData;
    };

    this.getFlow = function(){
        return FlowObject;
    };
});

myApp.service('ModalService', function($http, $timeout, $q, $rootScope, ContentService){
    var imageData = ContentService.getImageData();
    var maxsize = ContentService.maxSize();
    var loading = {del:false, img:false, edit:false};
    var info = {image:{}, index:0, newDesc:'', isEditing:false};

    this.right = function(){
        var self = this;
        info.index = info.index + 1;
        if(info.index == imageData.images.length){
            if(info.index < maxsize.images - 1){
                self.startLoad();
                ContentService
                    .moreUrls()
                    .then(function(result){
                        self.endLoad();
                        self.setImage();
                    });
                info.image = {};
            }
            else{
                info.index = imageData.images.length - 1;
            }
        }
        else{
            this.setImage();
        }
    };

    this.left = function(){
        info.index = info.index - 1;
        if(info.index == -1){
            info.index = 0;
        }
        this.setImage();
    };

    this.setImage = function(){
        imageData = ContentService.getImageData();
        info.image = imageData.images[info.index];
    };

    this.setIndex = function(val){
        if(info.isEditing){
            this.endEdit();
        }
        info.index = val;
        this.setImage();
    };

    this.refreshImage = function(){
        loading.img = false;
        var newImage = imageData.images[info.index];
        if((typeof newImage !== 'undefined') && ((!info.image.id) || (newImage.id === info.image.id))){
            this.setImage();
            return true;
        }
        else{
            var first = imageData.images[0];
            if((typeof first !== 'undefined') && (first.id === info.image.id)){
                info.index = 0;
                this.setImage();
                return true;
            }
            else{
                return false;
            }
        }
    };

    this.deleteImage = function(id){
        var deferred = $q.defer();
        loading.del = true;
        $http.delete('image',{params: {id: id}})
            .success(function(data){
                loading.del = false;
                loading.img = true;
                $timeout(function() {
                    ContentService
                        .updateTagsAndImages()
                        .then(function(result){loading.img = false; deferred.resolve(true);});
                } , 1000);
            });
        return deferred.promise;
    };

    this.updateImage = function(id, desc){
        loading.edit = true;
        var deferred = $q.defer();
        $http.put('image', {id: id, desc: desc})
            .success(function(data){
                loading.edit = false;
                info.isEditing = false;
                loading.img = true;
                $timeout(function() {
                    ContentService
                        .updateTagsAndImages()
                        .then(function(result){loading.img = false; deferred.resolve(true);});
                } , 1000);
            });
        return deferred.promise;
    };

    this.startEdit = function(){
        info.newDesc = info.image.desc;
        info.isEditing = true;
    };

    this.endEdit = function(){
        info.isEditing = false;
        info.newDesc = '';
    };


    this.isLoading = function(){
        return info.isEditing || loading.del || loading.img || loading.edit;
    };

    this.getImageData = function(){
        return imageData;
    };
    this.getMaxSize = function(){
        return maxsize;
    };

    this.getLoading = function(){
        return loading;
    };

    this.getInfo = function(){
        return info;
    };

    this.startLoad = function(){
        loading.img = true;
    };

    this.endLoad = function(){
        loading.img = false;
    };

    /** $rootScope.$on('full_load_start', function(event, args) {
        loading.img = true;
    });

     $rootScope.$on('url_load_done', function(event, args) {
        loading.img = false;
    });**/
});

myApp.controller('browseController', function($rootScope, $scope, $timeout, $modal, $location, $state, $anchorScroll, ContentService, screenSize, ModalService) {
        $scope.tagPageNumber = 1;
        $scope.tagPageSize = 12;
        $scope.imageData = ContentService.getImageData();
        $scope.maxSize = ContentService.maxSize();
        $scope.sort = ContentService.getSort();
        $scope.tagsReady = false;
        $scope.urlsReady = false;
        $scope.tagsLoading = false;
        $scope.urlsLoading = false;
        $scope.changeLoading = false;
        $scope.expand = {};
        $scope.edit = {};
        $scope.newDesc = {};

        $scope.refresh = function(){
            $scope.tagPageNumber = 1;
            ContentService.setRTSize($scope.tagPageSize*2);
            ContentService.setUsize(12);
            $scope.tagsLoaded = false;
            $scope.urlsLoaded = false;
        };

        $scope.addTag = function(tag){
            $scope.imageData.rqTags.push(tag);
            $timeout(function() {
                $state.go('home',{tags: $scope.imageData.rqTags.toString()});
            });
        };

        $scope.reload = function(){
            $scope.refresh();
            ContentService
                .updateTagsAndImages()
                .then(function(response){
                    $timeout(function(){
                        $location.hash('browse');
                        $anchorScroll();
                    });
                });
        };

        $scope.moreUrls = function(){
            $scope.urlsLoading = true;
            ContentService
                .moreUrls()
                .then(function(result){
                    $scope.urlsLoading = false;
                });
        };

        $scope.rqSlice = function(index){
            $timeout(function() {
                $state.go('home', {tags: $scope.imageData.rqTags.slice(0, index + 1).toString()});
            });
        };

        $scope.setFromRqString = function(rqString){
            $scope.refresh();
            if(typeof rqString === 'undefined'){
                rqString = "";
            }
            if(rqString.trim()){
                ContentService
                    .setRq(rqString.split(/[ ,]+/));
            }
            else {
                ContentService
                    .setRq([]);
            }
        };

        $scope.nextTagPage = function(){
            $scope.tagPageNumber = $scope.tagPageNumber + 1;
            var tt = ($scope.tagPageNumber + 1) * $scope.tagPageSize;
            if(ContentService.getRTSize() < tt){
                $scope.tagsLoading = true;
                ContentService.setRTSize(tt);
                ContentService
                    .updateTags()
                    .then(function(result){
                        $scope.tagsLoading = false;
                    });
            }
        };

        $scope.lastTagPage = function(){
            $scope.tagPageNumber = $scope.tagPageNumber - 1;
        };

        $scope.getTagPages = function(){
            var num =  Math.floor($scope.imageData.tags.length/$scope.tagPageSize) + 1;
            return (new Array(num));
        };

        $scope.getTagSlice = function(){
            return $scope.imageData.tags.slice(($scope.tagPageNumber-1)*$scope.tagPageSize, $scope.tagPageNumber*$scope.tagPageSize);
        };

        $scope.openImage = function(index, id){
            if($scope.mobile){
                $scope.expand[id] = !$scope.expand[id];
            }
            else{
                var modalInstance = $modal.open({
                    animation: false,
                    templateUrl: 'html/klymr/modal.html',
                    controller: 'ModalInstanceCtrl',
                    windowClass: 'app-modal',
                    size: 'lg',
                    resolve: {
                        index: function () {
                            return index;
                        }
                    }
                });
            }
        };

        $scope.toggleTagSort = function(){
            if($scope.sort.tags === 'Hits'){
                $scope.sort.tags = 'A-z';
            }
            else{
                $scope.sort.tags = 'Hits';
            }
            ContentService.updateTags();
        };

        $scope.toggleUrlSort = function(){
            if($scope.sort.urls === 'A-z'){
                $scope.sort.urls = 'Date';
            }
            else{
                $scope.sort.urls = 'A-z';
            }
            ContentService.updateUrls();
        };

        $scope.goToTags = function(rqString){
            angular.element("#st").blur();
            if(typeof rqString === 'undefined'){
                rqString = '';
            }
            $timeout(function(){
                $state.go('home', {tags: rqString.split(/[ ,]+/).toString()});
            });
            return true;
        };

        $scope.home = function(){
            if(($location.search().tags) && ($location.search().tags.length)){
                $rootScope.home = true;
                $scope.goToTags();
            }
        };

        $scope.delete = function(id){
            $scope.changeLoading = true;
            ModalService
                .deleteImage(id)
                .then(function(){
                    $scope.changeLoading = false;
                });
        };

        $scope.startEdit = function(id, desc){
            $scope.edit[id] = true;
            $scope.newDesc[id] = desc.valueOf();
        };

        $scope.endEdit = function(id){
            $scope.edit[id] = false;
            $scope.newDesc[id] = "";
        };

        $scope.update = function(id, desc){
            $scope.changeLoading = true;
            ModalService.updateImage(id, desc)
                .then(function(){
                    $scope.changeLoading = false;
                    if($scope.imageData.images[0].id === id){
                        $timeout(function(){$location.hash('0'); $anchorScroll();});
                    }
                });
        };

        $scope.getHref = function (ig) {
            if(ig.extUrl)
                return ig.extUrl;
            return ig.imageUrl;
        };
    
        $scope.$on('full_load_start', function(event, args){
            $scope.urlsReady = false;
            $scope.tagsReady = false;
        });

        $scope.$on('url_load_done', function(event, args){
            $scope.urlsReady = true;
        });

        $scope.$on('tag_load_done', function(event, args){
            $scope.tagsReady = true;
        });

        $scope.refresh();
        $scope.setFromRqString($location.search().tags);

        $rootScope.scroll = function(){
            return (typeof $location.search().tags !== 'undefined');
        };

        $scope.mobile = screenSize.on('xs, sm, md', function(match){
            $scope.mobile = match;
        });

    }

);

myApp.controller('addController', function($scope, AddService) {
        $scope.status = AddService.getStatus();
        $scope.uploadData = AddService.getUploadData();
        $scope.formData = AddService.getFormData();
        $scope.addError = AddService.getAddError();
        $scope.FlowObject = AddService.getFlow();

        $scope.flowSubmit = function(){
            AddService.flowSubmit();
        };

        $scope.upSuccess = function($file, $message){
            AddService.upSuccess($file, $message);
        };

        $scope.complete = function(){
            AddService.complete();
        };

        $scope.showLink = function(val){
            AddService.showLink(val);
        };

        $scope.checkFile = function($file){
            return AddService.checkFile($file);
        };

        $scope.cancel = function(){
            AddService.cancel();
        };

        $scope.submit = function(){
            AddService.submit();
        };
    }

);


myApp.controller('ModalInstanceCtrl', function($scope, $modalInstance, $timeout, $state, index, ModalService){
        ModalService.setIndex(index);

        $scope.imageData = ModalService.getImageData();
        $scope.maxsize = ModalService.getMaxSize();
        $scope.loading = ModalService.getLoading();
        $scope.info = ModalService.getInfo();

        $scope.isLoading = function(){
            return ModalService.isLoading();
        };

        $scope.deleteImage = function(id){
            ModalService
                .deleteImage(id)
                .then(function(result){
                    $modalInstance.dismiss("done");
                });
        };

        $scope.startEdit = function(){
            ModalService.startEdit();
        };

        $scope.updateImage = function(id, desc){
            ModalService
                .updateImage(id, desc)
                .then(function(result){
                    if(!ModalService.refreshImage()){
                        $modalInstance.dismiss("done");
                    }
                });
        };

        $scope.endEdit = function(){
            ModalService.endEdit();
        };

        $scope.right = function(){
            ModalService.right();
        };

        $scope.left = function(){
            ModalService.left();
        };

        $scope.goToTag = function(tag){
            $timeout(function(){
                $state.go('home', {tags: tag.split().toString()});
            });
            $modalInstance.dismiss("done");
        };
    }
);

myApp.controller('LoginController', function($scope, $state, principal){
    $scope.login = function() {
        FB.login(function (response){
            if (response.status === 'connected') {
                principal.authenticate(response.authResponse);
                if ($scope.returnToState)
                    $state.go($scope.returnToState.name, $scope.returnToStateParams);
                else $state.go('home');
            }
            else{
                principal.authenticate(null);
            }
        }, {scope: 'user_likes'});
    };
});

myApp.controller('InfoController', function($scope, principal){
    $scope.user =  principal.getAuth();
});
