myApp.controller('AbyssController', function($http, $interval, $timeout, $scope, $q, principal) {
    var abyss = this;
    abyss.images = [];
    abyss.channels = [];
    abyss.saving = {};
    abyss.saved = {};
    abyss.pageLoad = false;
    abyss.imageLoad = false;
    abyss.reverse = false;
    abyss.done = false;
    abyss.ucount = 10;
    abyss.pause = true;
    abyss.userId = principal.getAuth().userID;
    abyss.token = principal.getAuth().accessToken;

    this.getPages = function () {
        var deferred = $q.defer();
        abyss.pageLoad = true;
        $http.get('/channel', {params: {userId: abyss.userId}}).then(function (response) {
            abyss.channels = response['data'];
            abyss.chan = abyss.channels[0];
            abyss.pageLoad = false;
            deferred.resolve(true);
        }, function (response) {
            console.error(response);
            abyss.pageLoad = false;
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.getImages = function () {
        if(abyss.chan){
            abyss.imageLoad = true;
            $http.post('/fb/feed', {userid: abyss.userId, chid: abyss.chan.channelId, desc: abyss.chan.desc, token: abyss.token}).then(function (response) {
                abyss.images = response['data'];
                abyss.imageLoad = false;
            }, function (response) {
                console.error(response);
                abyss.imageLoad = false;
            });
        }

    };

    this.update = function () {
        abyss.pageLoad = true;
        $http({method : 'PUT', url : 'channel', data : abyss.chan,  headers: {'Content-Type': 'application/json'}})
            .then(function (response) {
                    abyss.pageLoad = false;
                },
                function (response) {
                    abyss.pageLoad = false;
                    console.log(response)
                });
    };

    this.save = function (image) {
        if (!(abyss.isLoading)){
            abyss.saving[image.obid] = true;
            $http.post('/abyss/image', image)
                .then(function (response) {
                        abyss.saving[image.obid] = false;
                        abyss.saved[image.obid] = true;
                        console.log(response)
                    },
                    function (response) {
                        abyss.saving[image.obid] = false;
                        abyss.saved[image.obid] = false;
                        console.log(response)
                    });
        }
    };

    this.togglePause = function () {
        abyss.chan.pause = !abyss.chan.pause;
        abyss.updateChan();
    };

    this.isSaving = function (obid) {
        return obid in abyss.saving && abyss.saving[obid];
    };


    this.isSaved = function (obid) {
        return obid in abyss.saved && abyss.saved[obid];
    };

    abyss.getPages()
         .then(function () {
            abyss.getImages();
        });
});

myApp.controller('AddController', function ($http, $q, principal) {
    var set = this;
    set.likes = [];
    set.adding = {};
    set.added = {};
    set.ucount = 10;
    set.done = false;
    set.isLoading = false;
    set.mode = 'Q';

    set.userId = principal.getAuth().userID;
    set.token = principal.getAuth().accessToken;

    this.isAdding = function (id) {
        return id in set.adding;
    };

    this.isAdded = function (id) {
        return id in set.added;
    };
    
    this.toggleMode = function () {
        if(set.mode == 'Q'){
            set.mode = 'L';
            set.getLikes();
        }
        else{
            set.mode = 'Q';
            set.likes = [];
        }
    };

    this.add = function (like, i) {
        angular.element("#st" + i).blur();
        set.adding[like.channelId] = true;
        like.userId = set.userId;
        like.pause = false;
        $http.post('/channel', like)
            .then(function (response) {
                    set.added[like.channelId] = true;
                },
                function (response) {
                    console.log(response);
                    set.added[like.channelId] = true;
                });
    };

    this.updateChannels = function (url, query) {
        set.isLoading = true;
        $http.post(url, {userId: set.userId, query: query, token: set.token}).then(function (response) {
            set.done = response['data'].length == set.likes.length;
            set.likes = response['data'];
            set.isLoading = false;
        }, function (response) {
            console.error(response);
            set.isLoading = false;
        });
    };

    this.getLikes = function () {
        this.updateChannels('/fb/likes', '');
    };

    this.getQuery = function (query) {
        if(query){
            this.updateChannels('/fb/query', query);
        }
    };


});

myApp.controller('RemoveController', function ($http, $q, $timeout, principal) {
    var set = this;
    set.likes = [];
    set.loading = {};
    set.ucount = 10;
    set.done = false;
    set.isLoading = false;
    set.isQuiet = false;
    set.userId = principal.getAuth().userID;

    this.isLoading = function (id) {
        return id in set.loading && set.loading[id];
    };

    this.remove = function (chan) {
        set.loading[chan.id] = true;
        $http({method : 'DELETE', url : 'channel', data : chan,  headers: {'Content-Type': 'application/json'}})
            .then(function (response) {
                    set.isQuiet = true;

                    $timeout(function(){
                        set.getPages()
                            .then(function(){
                                set.isQuiet = false;
                            })}, 1000);
                
                    set.loading[chan.id] = false;
                },
                function (response) {
                    console.log(response)
                });
    };

    this.update = function (chan) {
        set.loading[chan.id] = true;
        $http({method : 'PUT', url : 'channel', data : chan,  headers: {'Content-Type': 'application/json'}})
            .then(function (response) {
                    set.isQuiet = true;
                
                    $timeout(function(){ 
                        set.getPages()
                            .then(function(){
                                set.isQuiet = false;
                            })}, 1000);
                
                    set.loading[chan.id] = false;
                },
                function (response) {
                    console.log(response)
                });
    };

    this.getPages = function () {
        var deferred = $q.defer();
        set.isLoading = true;
        $http.get('/channel', {params: {userId: set.userId}}).then(function (response) {
            set.likes = response['data'];
            set.isLoading = false;
            deferred.resolve(true);
        }, function (response) {
            console.error(response);
            set.isLoading = false;
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    set.getPages();
});

myApp.controller('NavbarController', function ($state, $location) {
    var nvb = this;
    nvb.home = ($location.path() == '/abyss/home') || !$location.path();

    this.toggle = function () {
        nvb.home = !nvb.home;
        if(!nvb.home)
            $state.go('abyss.settings');
        else
            $state.go('abyss.home');
    };

    this.findBootstrapEnvironment = function () {
        var envs = ['xs', 'sm', 'md', 'lg'];

        var $el = $('<div>');
        $el.appendTo($('body'));

        for (var i = envs.length - 1; i >= 0; i--) {
            var env = envs[i];

            $el.addClass('hidden-'+env);
            if ($el.is(':hidden')) {
                $el.remove();
                return env;
            }
        }
    };
});
