var myApp = angular.module('myApp', ['ui.router','infinite-scroll','ui.bootstrap','flow','matchMedia']).config(function($sceProvider) {
    $sceProvider.enabled(false);
});

myApp.factory('principal', ['$q', '$http', '$timeout',
    function($q, $http, $timeout) {
        var _identity = undefined,
            _authenticated = null;

        return {
            isIdentityResolved: function() {
                return angular.isDefined(_identity);
            },
            isAuthenticated: function() {
                return _authenticated;
            },
            authenticate: function(identity) {
                _identity = identity;
                _authenticated = identity != null;
            },
            getAuth: function(){
                return _identity;
            },
            identity: function(force) {
                var deferred = $q.defer();

                if (force === true) _identity = undefined;

                if(typeof FB === 'undefined')  {
                    _authenticated = null;
                    deferred.resolve(_identity);
                    return deferred.promise;
                }


              if(!_identity) {
                  FB.getLoginStatus(function (response) {
                      if (response.status === 'connected') {
                          _identity = response.authResponse;
                          _authenticated = true;
                          FB.api('/me', function (response) {
                              _identity.name = response.name;
                              deferred.resolve(_identity);
                          });
                      } else {
                          _identity = null;
                          _authenticated = false;
                          deferred.resolve(_identity);
                      }
                  });

                  $timeout(function(){

                      if(_authenticated === null) {
                          _identity = null;
                          _authenticated = false;
                          deferred.resolve(_identity);
                      }
                  }, 3000);
              }

              else
                  deferred.resolve(_identity);

                return deferred.promise;
            }
        };
    }
]);

myApp.factory('authorization', ['$rootScope', '$state', 'principal',
    function($rootScope, $state, principal) {
        return {
            authorize: function() {

                return principal.identity()
                    .then(function() {
                        var isAuthenticated = principal.isAuthenticated();
                            if(isAuthenticated === null){
                                $rootScope.returnToState = $rootScope.toState;
                                $rootScope.returnToStateParams = $rootScope.toStateParams;
                                $state.go('lmao');
                            }
                            else if (!isAuthenticated) {
                                $rootScope.returnToState = $rootScope.toState;
                                $rootScope.returnToStateParams = $rootScope.toStateParams;
                                $state.go('login');
                            }
                    });
            }
        };
    }
]);

myApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('site', {
            'abstract': true,
            resolve: {
                authorize: ['authorization',
                    function(authorization) {
                        return authorization.authorize();
                    }
                ]
            }
        })
        .state('home', {
            url: '/?tags',
            cache: false,
            parent: 'site',
            reloadOnSearch: true,
            views:{
                'content@':{
                    templateUrl: 'html/klymr/home.html'
                },
                'info@home':{
                    templateUrl: 'html/klymr/infobar.html',
                    controller: 'InfoController'
                },
                'browse@home':{
                    templateUrl: 'html/klymr/browse.html',
                    controller: 'browseController'
                },
                'add@home':{
                    templateUrl: 'html/klymr/add.html',
                    controller: 'addController'
                }
            }
        })
        .state('login', {
            url: '/login',
            views: {
                'content@':{
                    templateUrl: 'html/klymr/login.html',
                    controller: 'LoginController'
                }
            }
        })
        .state('lmao',{
        views: {
            'content@':{
                templateUrl: 'html/klymr/loading.html'
            }
         }
        })
        .state('abyss', {
            url: "/abyss",
            parent: 'site',
            views: {
                'content@':{
                    templateUrl: "html/abyss/abyss.html"
                },
                'nav@abyss':{
                    templateUrl: "html/abyss/nav.html",
                    controller: "NavbarController",
                    controllerAs: "nvb"
                }
            }
        })
        .state('abyss.home',{
            url: "/home",
            views: {
                'main@abyss':{
                    templateUrl: "html/abyss/home.html",
                    controller: "AbyssController",
                    controllerAs: "abyss"
                }
            }
        })
        .state('abyss.settings', {
            url: "/settings",
            views: {
                'main@abyss':{
                    templateUrl: "html/abyss/settings.html"
                }
            }
        })
        .state('abyss.settings.add', {
            url: "/add",
            templateUrl: "html/abyss/add.html",
            controller: "AddController",
            controllerAs : "settings"
        })
        .state('abyss.settings.remove', {
            url: "/remove",
            templateUrl: "html/abyss/remove.html",
            controller: "RemoveController",
            controllerAs : "settings"
        })
    ;

});

myApp.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        target: '/upload',
        singleFile:true,
        permanentErrors: [404, 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        chunkSize: 5242880,
        simultaneousUploads: 4,
        testChunks:false
    };
    flowFactoryProvider.on('catchAll', function (event) {
    });
}]);


myApp.controller('HeaderController', function($state, $location){
    var header = this;
    header.pepe = $location.path().indexOf('abyss') == -1;
    
    this.toggle = function () {
        header.pepe = !header.pepe;

        if(header.pepe){
            $state.go('home');
        }
        else {
            $state.go('abyss.home');
        }
    }
});



myApp.run(['$window','$rootScope','$state', '$stateParams', '$location', 'authorization', 'principal',
    function($window,$rootScope,$state, $stateParams, $location, authorization, principal) {
        $window.fbAsyncInit = function() {
            FB.init({
                appId: '407115022821207',
                channelUrl: 'channel.html',
                status: true,
                cookie: true,
                xfbml: true,
                version: 'v2.4'
            });
            if ($rootScope.returnToState){
                $state.go($rootScope.returnToState, $rootScope.returnToStateParams);
            }
            else{
                $state.go('home');
            }
        };

        // Are you familiar to IIFE ( http://bit.ly/iifewdb ) ?

        (function(d){
            // load the Facebook javascript SDK

            var js,
                id = 'facebook-jssdk',
                ref = d.getElementsByTagName('script')[0];

            if (d.getElementById(id)) {
                return;
            }

            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            ref.parentNode.insertBefore(js, ref);

        }(document));
        $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
                $rootScope.toState = toState;
                $rootScope.toStateParams = toStateParams;
            });
    }]);