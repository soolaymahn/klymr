package com.abyss.api.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="URL invalid")
public class MediaRetrievalException extends RuntimeException {
}
