package com.abyss.api.services;

import com.abyss.api.model.AbyssImage;
import com.abyss.api.model.AstImageGhostEntity;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

public class TableService {
    private CloudTable imageGhostTable;

    public TableService(String storageConnectionString) {
        CloudStorageAccount storageAccount = null;
        try {
            storageAccount = CloudStorageAccount.parse(storageConnectionString);
            CloudTableClient tableClient = storageAccount.createCloudTableClient();
            imageGhostTable = tableClient.getTableReference("ghost");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void markSaved(AbyssImage abyssImage, String klymrId){
        updateImageStatus(abyssImage, klymrId, "SAVED");
    }

    private void updateImageStatus(AbyssImage abyssImage, String klymrId, String nst) {
        try {
            imageGhostTable.execute(TableOperation.insertOrReplace(
                    new AstImageGhostEntity(abyssImage.getUserid(), abyssImage.getObid(), abyssImage.getChid(), klymrId, nst)));
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }
}
