package com.abyss.api.services;


import com.abyss.api.errors.IndexCreationException;
import com.abyss.api.model.*;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.cardinality.Cardinality;
import org.elasticsearch.search.aggregations.metrics.cardinality.CardinalityBuilder;
import org.elasticsearch.search.sort.SortBuilder;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;


import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.elasticsearch.search.sort.SortBuilders.fieldSort;
import static org.elasticsearch.search.sort.SortOrder.*;

public class ElasticSearchService {
    private Client client;
    private String es_index;
    private String idx;
    private String fbc;

    public ElasticSearchService( Client client, String es_index, String idx, String fbc){
        this.client = client;
        this.es_index = es_index;
        this.idx = idx;
        this.fbc = fbc;
    }

    private List<String> getTags(String st){
        if(st.isEmpty()){
            return new ArrayList<>();
        }
        HashSet<String> tags = new HashSet<>();
        Arrays.asList(st.split("\\s+")).stream().filter(s -> s.charAt(0) == '#').forEach(tags::add);
        return new ArrayList<>(tags);
    }

    public String indexDocument(InternalDocument internalDocument){
        try {
            XContentBuilder builder = jsonBuilder()
                    .startObject()
                    .array("tags", internalDocument.getTags().toArray())
                    .field("image_url", internalDocument.getImageUrl())
                    .field("desc", internalDocument.getDesc())
                    .field("userId", internalDocument.getUserId());

            if(internalDocument instanceof ExternalDocument){
                builder.field("ext_url", ((ExternalDocument) internalDocument).getExtUrl());
            }

            if(internalDocument instanceof EmbedDocument){
                builder.field("embed_url", ((EmbedDocument) internalDocument).getEmbedUrl());
            }

            builder.endObject();

            String json =  builder.string();

            IndexResponse response = client.prepareIndex(es_index, idx)
                    .setSource(json)
                    .execute()
                    .actionGet();

            return response.getId();

        } catch (Exception e) {
            e.printStackTrace();
            throw new IndexCreationException();
        }
    }

    public String addChannel(FacebookChannel channel){
        try{
            XContentBuilder builder = jsonBuilder()
                    .startObject()
                    .array("tags", getTags(channel.getDesc()).toArray())
                    .field("userId", channel.getUserId())
                    .field("channelId", channel.getChannelId())
                    .field("name", channel.getName())
                    .field("rate", 0)
                    .field("state", "ON")
                    .endObject();

            String json =  builder.string();

            IndexResponse response = client.prepareIndex(es_index, fbc)
                    .setSource(json)
                    .execute()
                    .actionGet();

            return response.getId();
        }
        catch (Exception e) {
            throw new IndexCreationException();
        }
    }

    public List<FacebookChannel> getChannels(String userId){
        BoolQueryBuilder query = QueryBuilders.boolQuery().filter(QueryBuilders.termQuery("userId", userId));

        SearchRequestBuilder builder = client.prepareSearch(es_index)
                .setTypes(fbc)
                .addField("tags")
                .addField("userId")
                .addField("channelId")
                .addField("name")
                .addField("rate")
                .addField("state")
                .setFrom(0).setSize(100)
                .addSort(fieldSort("name"))
                .setPostFilter(query);

        SearchResponse response = builder.execute().actionGet();

        ArrayList<FacebookChannel> channels = new ArrayList<>();

        for(SearchHit hit : response.getHits()){
            StringBuilder sb = new StringBuilder();

            if(hit.field("tags") != null){
                hit.field("tags").getValues().stream().forEach(o -> {sb.append(o.toString()); sb.append(" ");});
            }

            channels.add(new FacebookChannel(
                    hit.getId(),
                    hit.field("channelId").getValue(),
                    hit.field("userId").getValue(),
                    sb.toString(),
                    hit.field("name").getValue(),
                    hit.field("rate").getValue(),
                    "ON".equals(hit.field("state").getValue())
                    ));
        }

        return channels;
    }

    public void deleteChannel(String id){
        DeleteResponse response = client.prepareDelete(es_index, fbc, id)
                .execute()
                .actionGet();
    }

    public void updateChannel(String id, String desc, Integer rate){
        try {
            UpdateResponse response = client
                    .prepareUpdate(es_index, fbc, id)
                    .setDoc(jsonBuilder()
                            .startObject()
                            .field("tags", getTags(desc).toArray())
                            .field("rate", rate)
                            .endObject())
                    .execute()
                    .actionGet();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public KlymrImageResponse getImagesFromTags(List<String> tags, Integer size, String sort, String userId){

        BoolQueryBuilder query = QueryBuilders.boolQuery().filter(QueryBuilders.termQuery("userId", userId));

        for(String tag: tags) {
            query = query.filter(QueryBuilders.termQuery("tags", tag));
        }

        SearchRequestBuilder builder = client.prepareSearch(es_index)
                                    .setTypes(idx)
                                    .addField("image_url").addField("ext_url").addField("embed_url")
                                    .addField("desc").addField("tags").addField("_timestamp")
                                    .setFrom(0).setSize(size)
                                    .setPostFilter(query);

        SearchResponse response = builder
                .addSort(getSort(sort))
                .execute().actionGet();

        ArrayList<InternalDocument> urls = new ArrayList<>();

        SearchHits hits = response.getHits();

        for(SearchHit hit : hits){
            if(hit.field("image_url") != null){
                List<String> tgs;

                if(hit.field("tags") != null) {
                    tgs = hit.field("tags").getValues().stream().map(Object::toString)
                            .collect(Collectors.toList());
                }

                else {
                    tgs = new ArrayList<>();
                }

                boolean ext = hit.getFields().containsKey("ext_url");
                boolean embed = hit.getFields().containsKey("embed_url");
                boolean image = hit.getFields().containsKey("image_url");

                String desc = hit.getFields().containsKey("desc") ? hit.field("desc").getValue() : "";

                if(ext && embed && image){
                    urls.add(new EmbedDocument(tgs, hit.field("image_url").getValue(), hit.field("ext_url").getValue(),
                            hit.field("embed_url").getValue(), desc, hit.getId(), userId));
                }

                else if(ext && image){
                    urls.add(new ExternalDocument(tgs, hit.field("image_url").getValue(), hit.field("ext_url").getValue(),
                            desc, hit.getId(), userId));
                }

                else if(image){
                    urls.add(new InternalDocument(tgs, hit.field("image_url").getValue(), desc, hit.getId(), userId));
                }
            }
        }

        return new KlymrImageResponse(urls, hits.getTotalHits());
    }

    private TagResponse getTagsFromTags(List<String> tags, Integer size, String sort, String userId){

        HashSet<String> tagSet = new HashSet<>();
        tagSet.addAll(tags);

        AggregationBuilder aggregation = AggregationBuilders.terms("all_tags").field("tags").order(getOrder(sort)).size(size);

        SearchResponse response = tagQuery(aggregation, tags, userId);

        Terms allTags = response.getAggregations().get("all_tags");

        List<Terms.Bucket> buckets = allTags.getBuckets();

        List<String> keys = buckets.stream()
                .map(MultiBucketsAggregation.Bucket::getKeyAsString)
                .filter(b -> !tagSet.contains(b))
                .collect(Collectors.toList());

        return new TagResponse(keys, Long.valueOf(0));
    }

    private Long getTagCount(List<String> tags, String userId){
        CardinalityBuilder aggregation = AggregationBuilders.cardinality("tags_count").field("tags");
        SearchResponse response = tagQuery(aggregation, tags, userId);
        Cardinality tagsCount = response.getAggregations().get("tags_count");
        return Math.max(tagsCount.getValue() - tags.size(), 0);
    }

    private SearchResponse tagQuery(AbstractAggregationBuilder aggregation, List<String> tags, String userId){

        BoolQueryBuilder query = QueryBuilders.boolQuery().filter(QueryBuilders.termQuery("userId", userId));

        for(String tag: tags) {
            query = query.filter(QueryBuilders.termQuery("tags", tag));
        }

        SearchRequestBuilder builder = client.prepareSearch(es_index)
                .setTypes(idx)
                .addAggregation(aggregation)
                .setQuery(query);

        return builder.execute().actionGet();
    }

    public TagResponse fillTagsFromTags(List<String> tags, Integer size, String sort, String userId){
        Integer rqSize = size;
        Long total = getTagCount(tags,userId);
        TagResponse response;
        do{
            response = getTagsFromTags(tags, rqSize, sort, userId);
            rqSize += 10;
        }
        while ((response.getContent().size() < size) && (response.getContent().size() < total));
        response.setTotal(total);
        return response;
    }

    public void deleteKlymrImage(String id){
        DeleteResponse response = client.prepareDelete(es_index, idx, id)
                .execute()
                .actionGet();
    }

    public void updateImage(String id, String desc){
        try {
            UpdateResponse response = client
                    .prepareUpdate(es_index, idx, id)
                    .setDoc(jsonBuilder()
                            .startObject()
                            .field("tags", getTags(desc).toArray())
                            .array("desc", desc)
                            .endObject())
                    .execute()
                    .actionGet();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private SortBuilder getSort(String sortName){
        if(sortName == null)
            return fieldSort("_timestamp").order(DESC);
        else {
            switch (sortName){
                case "A-z":
                    return fieldSort("desc").order(ASC);
                case "Date":
                    return fieldSort("_timestamp").order(DESC);
                default:
                    return fieldSort("_timestamp").order(DESC);
            }
        }
    }

    private Terms.Order getOrder(String order) {
        if(order == null){
            return Terms.Order.count(false);
        }
        else {
            switch (order){
                case "Hits":
                    return Terms.Order.count(false);
                case "A-z":
                    return Terms.Order.term(true);
                default:
                    return Terms.Order.count(false);
            }
        }
    }

    public void close(){
        client.close();
    }
}
