package com.abyss.api.services;


import com.abyss.api.errors.MediaRetrievalException;
import com.abyss.api.model.DocumentRequest;
import com.abyss.api.model.EmbedDocument;
import com.abyss.api.model.ExternalDocument;
import com.abyss.api.model.InternalDocument;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;

public class DocumentService {
    @Autowired
    private AmazonS3Client amazonS3Client;

    private final String BUCKET_NAME = "klymr-fs";
    private final String DIRECTORY = "images";
    private final String HOST = "https://s3-us-west-1.amazonaws.com/";
    private final String DEFAULT_LINK_IMAGE = "https://s3-us-west-1.amazonaws.com/klymr-fs/link.png";

    public String tempUpload(MultipartFile file) throws Exception{
            String ext = mimeToExt(file.getContentType());
            String uuid = UUID.randomUUID().toString();
            File temp = File.createTempFile("upload", ext);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(temp));
            stream.write(file.getBytes());
            stream.close();
            amazonS3Client.putObject(new PutObjectRequest(BUCKET_NAME, getKey(DIRECTORY,uuid,ext), temp).withCannedAcl(CannedAccessControlList.PublicRead));
            temp.delete();
            return getAWSURL(uuid, ext);
    }

    private String mimeToExt(String type){
        return "." + type.substring("image/".length());
    }

    private String getUrlExt(String url) throws Exception{
        String mimeType = getMimeType(url);

        if(Arrays.asList("image/jpeg", "image/jpg", "image/gif", "image/png").contains(mimeType)){
            return mimeToExt(mimeType);
        }

        return null;
    }

    private String getMimeType(String urlname) throws Exception {
        URL url = new URL(urlname);
        HttpURLConnection connection = (HttpURLConnection)  url.openConnection();
        connection.setRequestMethod("HEAD");
        connection.connect();
        return connection.getContentType();
    }

    private void resizeImage(File input, String outputImagePath, int scaledWidth, int scaledHeight) throws Exception{
        BufferedImage inputImage = ImageIO.read(input);

        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }

    public InternalDocument buildDocument(DocumentRequest dr) throws Exception{
        if (dr.getDesc() == null)
            dr.setDesc("");

        if(dr.getType() == null)
            dr.setType("ANY");

        List<String> tags = getTags(dr.getDesc());

        if(dr.isSaved()) {
            dr.setImageUrl(dr.getUrl());
            return new InternalDocument(dr, tags);
        }

        switch (dr.getType()){
            case "ANY":
                if(!(buildDocumentFromImage(dr) || buildExternalDocument(dr)))
                    throw new MediaRetrievalException();
                break;
            case "IMAGE":
                if(!buildDocumentFromImage(dr))
                    throw new MediaRetrievalException();
                break;
            case "EXT":
                if(!buildExternalDocument(dr))
                    throw new MediaRetrievalException();
                break;
        }

        if((dr.getEmbedUrl() != null) && (dr.getExtUrl() != null) && (dr.getImageUrl() != null))
            return new EmbedDocument(dr, tags);

        else if((dr.getExtUrl() != null) && (dr.getImageUrl() != null))
            return new ExternalDocument(dr, tags);

        else if(dr.getImageUrl() != null)
            return new InternalDocument(dr, tags);

        throw new MediaRetrievalException();
    }



    private String saveImage(String url, Boolean shrink) throws Exception{
        String uuid = UUID.randomUUID().toString();

        String imageExt = getUrlExt(url);

        if(imageExt != null){
            Path temp = Paths.get("/tmp/" + uuid + imageExt);
            Files.copy(new URL(url).openStream(), temp);

            if(shrink){
                String shrinkPath = "/tmp/shr" + uuid + imageExt;
                resizeImage(temp.toFile(), shrinkPath, 300, 300);
                temp = Paths.get(shrinkPath);
            }

            return writeToS3(temp.toFile(), uuid, imageExt);
        }

        return null;
    }

    private boolean buildDocumentFromImage(DocumentRequest dr){
        String imageUrl = null;

        try{
            imageUrl = saveImage(dr.getUrl(), false);
        } catch (Exception ignored) { }

        if(imageUrl != null){
            dr.setImageUrl(imageUrl);
            return true;
        }

        return false;
    }

    private boolean buildExternalDocument(DocumentRequest dr){
        if(!isValidUrl(dr.getUrl()))
            return false;

        dr.setExtUrl(dr.getUrl());
        dr.setImageUrl(DEFAULT_LINK_IMAGE);

        try {
            Document doc = Jsoup
                    .connect(dr.getUrl())
                    .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36")
                    .get();
            Elements metaTags = doc.getElementsByTag("meta");


            for (Element metaTag : metaTags) {
                String content = metaTag.attr("content");
                String name = metaTag.attr("property");

                if (name.equals("og:image")) {
                    try{
                        String imageUrl = saveImage(content, true);
                        if(imageUrl != null)
                            dr.setImageUrl(imageUrl);
                    } catch (Exception ignored) {}
                }

                if (name.equals("og:title")) {
                    dr.setDesc(content + " " + dr.getDesc());
                }

                if(name.equals("og:video:url") && (dr.getEmbedUrl() == null)){
                    dr.setEmbedUrl(content);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }

    private boolean isValidUrl(String u){
        try{
            new URL(u);
            return true;
        }
        catch (MalformedURLException ignored){
            return false;
        }
    }

    private List<String> getTags(String st){
        if(st.isEmpty()){
            return new ArrayList<>();
        }
        HashSet<String> tags = new HashSet<>();
        Arrays.asList(st.split("\\s+")).stream().filter(s -> s.charAt(0) == '#').forEach(tags::add);
        return new ArrayList<>(tags);
    }

    private String writeToS3(File temp, String uuid, String ext){
        amazonS3Client.putObject(new PutObjectRequest(BUCKET_NAME, getKey(DIRECTORY, uuid,ext), temp).withCannedAcl(CannedAccessControlList.PublicRead));
        temp.delete();
        return getAWSURL(uuid, ext);
    }

    private String getAWSURL(String uuid, String ext){
        return HOST + BUCKET_NAME+"/" + DIRECTORY + "/" + uuid + ext;
    }

    private String getAWSURL(String fname){
        return HOST + BUCKET_NAME+"/" + DIRECTORY + "/" + fname;
    }

    private String getKey(String directory, String uuid, String ext){
        return directory + "/" + uuid + ext;
    }

    private String getKey(String directory, String fname){
        return directory + "/" + fname;
    }

}
