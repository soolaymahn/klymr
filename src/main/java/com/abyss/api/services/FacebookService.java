package com.abyss.api.services;

import com.abyss.api.model.AbyssImage;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.abyss.api.model.FacebookChannel;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FacebookService {
    private String baseUrl;

    public FacebookService(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public List<FacebookChannel> search(String token, String query, Set<String> exclude){
        try{
            query = URLEncoder.encode(query, "UTF-8");
        }
        catch (Exception e){
            return new ArrayList<>();
        }

        String url = baseUrl + "search?access_token=" + token + "&q=" + query + "&type=page";
        return getChannels(url, exclude);
    }

    public List<FacebookChannel> getLikes(String token, Set<String> exclude){
        String url = baseUrl + "me/likes?access_token=" + token;
        return getChannels(url, exclude);
    }

    private List<FacebookChannel> getChannels(String url, Set<String> exclude){
        ArrayList<FacebookChannel> ret = new ArrayList<>();
        try {
            JSONObject jsonObj = readJson(url);
            JSONArray likes = jsonObj.getJSONArray("data");
            for(int i = 0; i < likes.length(); i++){
                JSONObject like = likes.getJSONObject(i);
                if(!exclude.contains(like.getString("id"))){
                    ret.add(new FacebookChannel("", like.getString("id"), "", "", like.getString("name"), 0, false));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;

    }

    public List<AbyssImage> readFeed(String token, String chid, String desc, String userid){
        String url = baseUrl + chid + "/posts?access_token=" + token + "&fields=type,object_id,from";
        ArrayList<AbyssImage> ret = new ArrayList<>();
        try {
            JSONObject jsonObj = readJson(url);
            JSONArray likes = jsonObj.getJSONArray("data");
            for(int i = 0; i <= likes.length(); i++){
                if(ret.size() >= 10)
                    break;
                try{
                    JSONObject item = likes.getJSONObject(i);
                    if(item.getString("type").equals("photo") && item.getJSONObject("from").getString("id").equals(chid)){
                        String obid = item.getString("object_id");
                        String photoUrl = baseUrl + obid + "?access_token=" + token + "&fields=images";

                        JSONObject photo = readJson(photoUrl);
                        ArrayList<String> urls = new ArrayList<>();
                        JSONArray images = photo.getJSONArray("images");

                        for(int y = 0; y <= Math.min(images.length(), 5); y++){
                            try{
                                urls.add(images.getJSONObject(y).getString("source"));
                            } catch (Exception ignored){}
                        }

                        ret.add(new AbyssImage(obid, chid, desc, userid, urls));
                    }
                }
                catch (Exception ignored){}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;

    }

    private JSONObject readJson(String url) throws IOException, JSONException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(new HttpGet(url));
        return new JSONObject(EntityUtils.toString(response.getEntity()));
    }
}
