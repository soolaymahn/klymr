package com.abyss.api.controller;

import com.abyss.api.services.*;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@Configuration
public class Application  extends SpringBootServletInitializer {
    private String ACCESS_KEY =  "AKIAIK7AXWJCBJ5ZJ27Q";
    private String SECRET_KEY = "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";

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public ElasticSearchService elasticSearchService() {
        TransportClient client = null;
        try {
            client = TransportClient.builder().build()
                    .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("159.203.246.62"), 9300));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return new ElasticSearchService(client, "klymr", "idx", "fbc");
    }

    @Bean
    public DocumentService fileUploadService(){
        return new DocumentService();
    }

    @Bean
    public AmazonS3Client amazonS3Client(){
        AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
        return new AmazonS3Client(credentials);
    }

    @Bean
    public TableService imagesService() {
        String conns =  "DefaultEndpointsProtocol=http;AccountName=metalmonster;AccountKey=5Me7n7LIkjzxsYyp5SlMU+8Pa7IbADL0AL4FY6Fq8QC1wdeaYLOmZPBwOIZ/E/SbhkhbGREdCcuUOE8NzRDGvg==";
        return new TableService(conns);
    }


    @Bean
    public FacebookService facebookService() {
        String url = "https://graph.facebook.com/v2.6/";
        return new FacebookService(url);
    }
}
