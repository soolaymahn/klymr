package com.abyss.api.controller;

import com.abyss.api.errors.MediaRetrievalException;
import com.abyss.api.errors.NoUrlException;
import com.abyss.api.model.*;
import com.abyss.api.services.ElasticSearchService;
import com.abyss.api.services.FacebookService;
import com.abyss.api.services.DocumentService;

import com.abyss.api.services.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PreDestroy;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class KlymrController {
    @Autowired
    private ElasticSearchService elasticSearchService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private FacebookService fbService;

    @Autowired
    private TableService tableService;

    @RequestMapping(value = "/tags/images/urls", method = RequestMethod.POST)
    public KlymrImageResponse swank(@RequestBody TagRequest rq){
        return elasticSearchService.getImagesFromTags(rq.getTags(), rq.getSize(), rq.getSort(), rq.getUserId());
    }

    @RequestMapping(value = "/tags/images/tags", method = RequestMethod.POST)
    public TagResponse swank2(@RequestBody TagRequest rq){
        return elasticSearchService.fillTagsFromTags(rq.getTags(), rq.getSize(), rq.getSort(), rq.getUserId());
    }

    @RequestMapping(value = "/image", method = RequestMethod.POST)
    public NewIndexResponse addToKlymr(@RequestBody DocumentRequest documentRequest){
        if((documentRequest.getUrl() == null) || (documentRequest.getUrl().isEmpty())){
            throw new NoUrlException();
        }

        try {
            InternalDocument doc = documentService.buildDocument(documentRequest);
            return new NewIndexResponse(elasticSearchService.indexDocument(doc));
        }
        catch (Exception e){
            e.printStackTrace();
        }

        throw new MediaRetrievalException();
    }

    @RequestMapping(value = "/image", method = RequestMethod.DELETE)
    public void delete(@RequestParam("id") String id ){
        elasticSearchService.deleteKlymrImage(id);
    }

    @RequestMapping(value = "/image", method = RequestMethod.PUT)
    public void update(@RequestBody UpdateRequest uq){
        elasticSearchService.updateImage(uq.getId(), uq.getDesc());
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String handleFileUpload(@RequestParam("file") MultipartFile file) {
        String url;
        try{
            url = documentService.tempUpload(file);
        }
        catch (Exception e){
            throw new MediaRetrievalException();
        }
        return url;
    }

    @RequestMapping(value = "/abyss/image", method = RequestMethod.POST)
    public NewIndexResponse sweg(@RequestBody AbyssImage abyssImage){
        for(String url : abyssImage.getUrls()){
            try{
                DocumentRequest dr = new DocumentRequest(abyssImage.getDesc(), url, false, abyssImage.getUserid());
                dr.setType("IMAGE");

                InternalDocument doc = documentService.buildDocument(dr);
                String kid = elasticSearchService.indexDocument(doc);
                tableService.markSaved(abyssImage, kid);

                return new NewIndexResponse(kid);
            }
            catch (Exception ignored){ }
        }

        throw new MediaRetrievalException();
    }

    @RequestMapping(value = "/channel", method = RequestMethod.GET)
    public List<FacebookChannel> demboi(@RequestParam String userId){
        return elasticSearchService.getChannels(userId);
    }

    @RequestMapping(value = "/channel", method = RequestMethod.POST)
    public NewIndexResponse murica(@RequestBody FacebookChannel facebookChannel){
        return new NewIndexResponse(elasticSearchService.addChannel(facebookChannel));
    }

    @RequestMapping(value = "/channel", method = RequestMethod.PUT)
    public void murica2(@RequestBody FacebookChannel facebookChannel){
        elasticSearchService.updateChannel(facebookChannel.getId(), facebookChannel.getDesc(), facebookChannel.getRate());
    }

    @RequestMapping(value = "/channel", method = RequestMethod.DELETE)
    public void warofnorthernagression(@RequestBody FacebookChannel facebookChannel) {
        elasticSearchService.deleteChannel(facebookChannel.getId());
    }

    @RequestMapping(value = "/fb/likes", method = RequestMethod.POST)
    public List<FacebookChannel> datboi(@RequestBody GetFbPagesRequest request){
        Set<String> al = elasticSearchService.getChannels(request.getUserId()).stream().map(FacebookChannel::getChannelId).collect(Collectors.toSet());
        return fbService.getLikes(request.getToken(), al);
    }

    @RequestMapping(value = "/fb/query", method = RequestMethod.POST)
    public List<FacebookChannel> yeezy(@RequestBody GetFbPagesRequest request){
        Set<String> al = elasticSearchService.getChannels(request.getUserId()).stream().map(FacebookChannel::getChannelId).collect(Collectors.toSet());
        return fbService.search(request.getToken(), request.getQuery(), al);
    }

    @RequestMapping(value = "/fb/feed", method = RequestMethod.POST)
    public List<AbyssImage> rickjames(@RequestBody GetFbFeedRequest request){
        return fbService.readFeed(request.getToken(), request.getChid(), request.getDesc(), request.getUserid());
    }

    @PreDestroy
    public void destroy(){
        elasticSearchService.close();
        System.out.println("Gone");
    }
}
