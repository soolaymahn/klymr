package com.abyss.api.model;

import java.util.List;

public class TagResponse implements KlymrResponse {
    private List<String> content;
    private Long total;

    public TagResponse(List<String> content, Long total) {
        this.content = content;
        this.total = total;
    }

    public TagResponse(){

    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }

    @Override
    public Long getTotal() {
        return total;
    }

    @Override
    public void setTotal(Long total) {
        this.total = total;
    }
}
