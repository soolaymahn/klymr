package com.abyss.api.model;

/**
 * Created by mbasrai on 9/6/16.
 */
public class AddGroupRequest {
    private String token;
    private FacebookChannel group;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public FacebookChannel getGroup() {
        return group;
    }

    public void setGroup(FacebookChannel group) {
        this.group = group;
    }
}
