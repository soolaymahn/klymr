package com.abyss.api.model;

import java.util.List;

public class AbyssImageResponse {
    private List<AbyssImage> images;
    private boolean isDone;

    public AbyssImageResponse(List<AbyssImage> images, Boolean isDone) {
        this.images = images;
        this.isDone = isDone;
    }

    public AbyssImageResponse() {

    }

    public List<AbyssImage> getImages() {
        return images;
    }

    public void setImages(List<AbyssImage> images) {
        this.images = images;

    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
}


