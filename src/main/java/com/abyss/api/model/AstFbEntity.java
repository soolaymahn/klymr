package com.abyss.api.model;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class AstFbEntity extends TableServiceEntity {
    private String Tags;
    private String Name;
    private String Rate;
    private String Pause;

    public String getTags() {
        return Tags;
    }

    public void setTags(String tags) {
        this.Tags = tags;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getPause() {
        return Pause;
    }

    public void setPause(String pause) {
        Pause = pause;
    }

    private AstFbEntity(String userId, String rowKey, String tags, String name, Integer rate, Boolean pause) {
        super(userId, rowKey);
        this.Tags = tags;
        this.Name = name;
        this.Rate = rate.toString();
        this.Pause = pause ? "true" : "false";
    }

    public AstFbEntity(FacebookChannel facebookChannel){
        this(facebookChannel.getUserId(), facebookChannel.getId(), facebookChannel.getDesc(), facebookChannel.getName(), facebookChannel.getRate(), facebookChannel.getPause());
    }

    public AstFbEntity() {}

}
