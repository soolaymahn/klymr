package com.abyss.api.model;

public class FacebookChannel {
    private String id;
    private String channelId;
    private String userId;
    private String desc;
    private String name;
    private Integer rate;
    private Boolean pause;

    public FacebookChannel() {
    }

    public FacebookChannel(String id, String channelId, String userId, String desc, String name, Integer rate, Boolean pause) {
        this.id = id;
        this.channelId = channelId;
        this.userId = userId;
        this.desc = desc;
        this.name = name;
        this.rate = rate;
        this.pause = pause;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Boolean getPause() {
        return pause;
    }

    public void setPause(Boolean pause) {
        this.pause = pause;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
