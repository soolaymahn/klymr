package com.abyss.api.model;

import java.util.List;

public interface KlymrResponse {
    Long getTotal();
    void setTotal(Long total);
}
