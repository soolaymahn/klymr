package com.abyss.api.model;

import java.util.List;

public class EmbedDocument extends ExternalDocument {
    private String embedUrl;

    public EmbedDocument() {
    }

    public EmbedDocument(List<String> tags, String imageUrl, String extUrl, String embedUrl, String desc, String id, String userId) {
        super(tags, imageUrl, extUrl, desc, id, userId);
        this.embedUrl = embedUrl;
    }

    public EmbedDocument(DocumentRequest dr, List<String> tags){
        super(dr, tags);
        this.embedUrl = dr.getEmbedUrl();
    }

    public String getEmbedUrl() {
        return embedUrl;
    }

    public void setEmbedUrl(String embedUrl) {
        this.embedUrl = embedUrl;
    }
}
