package com.abyss.api.model;

import java.util.List;

public class ExternalDocument extends InternalDocument {
    private String extUrl;

    public ExternalDocument() {
    }

    public ExternalDocument(List<String> tags, String imageUrl, String extUrl, String desc, String id, String userId) {
        super(tags,imageUrl, desc, id, userId);
        this.extUrl = extUrl;
    }

    public ExternalDocument(DocumentRequest dr, List<String> tags) {
        super(dr, tags);
        this.extUrl = dr.getExtUrl();
    }

    public String getExtUrl() {
        return extUrl;
    }

    public void setExtUrl(String extUrl) {
        this.extUrl = extUrl;
    }
}
