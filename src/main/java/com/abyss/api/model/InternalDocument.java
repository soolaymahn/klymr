package com.abyss.api.model;

import java.util.ArrayList;
import java.util.List;

public class InternalDocument {
    private List<String> tags;
    private String imageUrl;
    private String desc;
    private String id;
    private String userId;

    public InternalDocument(){

    }

    public InternalDocument(List<String> tags, String imageUrl, String desc, String id, String userId) {
        this.tags = tags;
        this.imageUrl = imageUrl;
        this.desc = desc;
        this.id = id;
        this.userId = userId;
    }

    public InternalDocument(DocumentRequest dr, List<String> tags) {
        this.tags = tags;
        this.imageUrl = dr.getImageUrl();
        this.desc = dr.getDesc();
        this.userId = dr.getUserId();
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
