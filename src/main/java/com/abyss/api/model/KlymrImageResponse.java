package com.abyss.api.model;

import java.util.List;

public class KlymrImageResponse implements KlymrResponse {
    private Long total;
    private List<InternalDocument> content;

    public KlymrImageResponse(List<InternalDocument> content, Long total) {
        this.total = total;
        this.content = content;
    }

    public KlymrImageResponse() {
    }

    @Override
    public Long getTotal() {
        return total;
    }

    @Override
    public void setTotal(Long total) {
        this.total = total;
    }

    public List<InternalDocument> getContent() {
        return content;
    }

    public void setContent(List<InternalDocument> content) {
        this.content = content;
    }
}
