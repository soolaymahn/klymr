package com.abyss.api.model;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class AstImageGhostEntity extends TableServiceEntity {
    private String Status;
    private String ChannelId;
    private String KlymrId;

    public AstImageGhostEntity() {
    }

    public AstImageGhostEntity(String partitionKey, String rowKey, String channelId, String klymrId, String status) {
        super(partitionKey, rowKey);
        this.ChannelId = channelId;
        this.Status = status;

        if((klymrId != null) && !klymrId.equals(""))
            this.KlymrId = klymrId;
    }

    public String getChannelId() {
        return ChannelId;
    }

    public void setChannelId(String channelId) {
        ChannelId = channelId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getKlymrId() {
        return KlymrId;
    }

    public void setKlymrId(String klymrId) {
        KlymrId = klymrId;
    }
}
