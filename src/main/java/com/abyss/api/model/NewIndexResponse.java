package com.abyss.api.model;

public class NewIndexResponse {
    private String id;

    public NewIndexResponse(String id) {
        this.id = id;
    }

    public NewIndexResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
