package com.abyss.api.model;

import com.mongodb.DBObject;

import java.util.List;

public class AbyssImage {
    private String obid;
    private String desc;
    private List<String> urls;
    private String chid;
    private String userid;

    public AbyssImage(){

    }

    public AbyssImage(String obid, String chid, String desc, String userid, List<String> urls) {
        this.obid = obid;
        this.chid = chid;
        this.urls = urls;
        this.desc = desc;
        this.userid = userid;
    }

    public String getObid() {
        return obid;
    }

    public void setObid(String obid) {
        this.obid = obid;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getChid() {
        return chid;
    }

    public void setChid(String chid) {
        this.chid = chid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}

